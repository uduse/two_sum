#include "stdafx.h"
#include "HashTable.h"
#include <math.h>
#include <iostream>

bool prime( long long x )
{
	if ( x < 2 ) return false;
	for ( long long i = 2; i <= sqrt( x ); i++ )
	{
		if ( ( x%i ) == 0 ) return false;
	}
	return true;
}

long long nextPrime( long long x )
{
	while ( !prime( x ) )
	{
		x++;
	}
	return x;
}

HashTable::HashTable( long long size ):tableSize( nextPrime( size ) )
{
	table = new long long[tableSize];
	for ( long long i = 0; i < tableSize; i++ )
	{
		table[i] = EMPTY;
	}
}

HashTable::~HashTable()
{
}

bool HashTable::insert( long long x )
{
	long long* posValue = &table[findPos( x )];

	// if socket empty, then insert
	if ( *posValue == EMPTY ) // item not found
	{
		*posValue = x;
		return true;
	}
	else // else already there
	{
		return false;
	}
	return false;
}


bool HashTable::find( long long key )
{
	long long pos = findPos( key );
	if ( table[pos] == EMPTY)
	{
		return false;
	}
	else
	{
		return true;
	}
}

long long HashTable::hash( long long key )
{
	return key % tableSize;
}

long long HashTable::findPos( long long key )
{
	long long pos = hash( key );

	while ( table[pos] != EMPTY && table[pos] != key )
	{
		pos++;
	}

	return pos;
}
