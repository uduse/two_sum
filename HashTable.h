#pragma once
#include <limits.h>

#define EMPTY LLONG_MAX

class HashTable
{
public:
	HashTable( long long size );
	~HashTable();

	bool insert( long long key );
	bool find( long long key );

private:
	long long* table;
	long long tableSize;

	long long hash( long long key );
	long long findPos( long long key );
};

