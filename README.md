# Two Sum

Implementation of an algorithm that computes the number of target values **t** in the interval \[-10000,10000] (inclusive) such that there are distinct numbers **x**, **y** in the input file that satisfy **x + y = t**

<br>

## Things I learned from this project
2. **Set** in STL is fantastic int terms of sorting and eliminating duplicates. However, its iterator is super duper slow compared to direct indexing. 