// Two_Sum.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "HashTable.h"
#include <iostream>
#include <fstream>
#include <set>
#include <time.h>
#include <algorithm>

using namespace std;

int numInput = 0;
const int tableSize = 20001;
bool kTable[tableSize]{ false };

int getCount();

int getCount()
{
	int count = 0;
	for ( int index = 0; index < tableSize; index++ )
	{
		if ( kTable[index] )
		{
			count++;
		}
	}
	return count;
}

int setTable( long long *longArray )
{

	int numExecute = 0;

	int index = 0;

	int xitr = 0;
	int yitr = 0;

	// Set upper bound
	long long upBound = 10000 - longArray[xitr];
	yitr = numInput - 1;
	while ( true )
	{
		if ( longArray[yitr] > upBound )
		{
			--yitr;
		}
		else
		{
			break;
		}
	}
	int yMax = min( yitr + 1, numInput - 1 ); // Reset upper bound


	// Check each kValue
	while ( index < tableSize )
	{

		// find next false
		while ( kTable[index] )
		{
			index++;
			if ( index >= tableSize )
			{
				break;
			}
		}

		if ( index % 100 == 0 )
		{
			cout << "index: " << index << endl;
			cout << getCount() << endl;
		}

		long long kValue = index - 10000;

		xitr = 0;
		yitr = yMax;


		upBound = 10000 - longArray[xitr];

		while ( true )
		{
			if ( longArray[yitr] > upBound )
			{
				--yitr;
			}
			else
			{
				break;
			}
		}


		while ( true )
		{
			int currentSum = longArray[xitr] + longArray[yitr];

			if ( currentSum >= -10000 && currentSum <= 10000 )
			{
				kTable[currentSum + 10000] = true;
			}

			if ( currentSum > kValue )
			{
				--yitr;
			}
			else if ( currentSum < kValue )
			{
				++xitr;
			}
			else
			{
				kTable[kValue + 10000] = true;
				break;
			}

			if ( xitr >= yitr )
			{
				break;
			}

		}

		index++;
	}

	return getCount();
}

int _tmain( int argc, _TCHAR* argv[] )
{

	//HashTable valueTable( 1000000 );
	//HashTable sumTable( 1000 );

	set<long long> mySet;

	long long myLong;
	//fstream input( "1.txt" );
	//fstream input( "2.txt" );
	//fstream input( "3.txt" );
	fstream input( "Test.txt" );


	while ( input >> myLong )
	{
		numInput++;
		mySet.insert( myLong );
	}

	//====================================//
	//  Transfer to dynamic array
	//====================================//

	long long *myTable = new long long[numInput];
	set<long long>::iterator itr;
	int count = 0;
	for ( itr = mySet.begin(); itr != mySet.end(); itr++ )
	{
		myTable[count] = *itr;
		count++;
	}

	numInput = count;

	clock_t begin = clock();

	cout << "count: " << setTable( myTable ) << endl;

	clock_t end = clock();
	double elapsed_secs = double( end - begin ) / CLOCKS_PER_SEC;
	cout << endl;
	cout << "Running Time: " << elapsed_secs; cout << endl;

	return 0;
}